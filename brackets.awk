#!/bin/awk -f

# this script is for extracting yaml objects from terraform modules
# for example, to extract a module named "vsphere_vms",
# run the following command: 
#
# $ awk -f brackets.awk -v searchstring='module vsphere_vms' target.tf
#
# note that the "{" is not required, 
# and that the search-string is in single-qoutes

BEGIN{
    bracket_count=1
    searchstring = gensub(/( *)({)/, "", "g", searchstring)
    searchstring = searchstring " {"
    searchstring = gensub(/( +)/, "( *)", "g", searchstring)
}
$0~searchstring{
    print NR OFS $0
    start_block = NR
    getline
    while(bracket_count != 0){
        print NR $0
        if( $0 ~ "{" ){
            bracket_count++
        }
        if( $0 ~ "}" ){
            bracket_count--
        }
        getline    
    }
    end_block = NR
    NR = start_block
    bracket_count=1 
}
END{

}
