#!/usr/bin/python3

import requests
import json
import colorama
import urllib3
import time
import os
import sys
import subprocess
import sys


try :
    GITLAB_API_TOKEN = os.environ["GITLAB_API_TOKEN"]
except (KeyError) :
    GITLAB_API_TOKEN = input('enter Gitlab API token (or define GITLAB_API_TOKEN in environment variable)\n')


### Variables ###
    
gitlab_url_base = 'https://gitlab.com/api/v4'

colorama.init(autoreset=True)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


session_gitlab = requests.session()

def map_object(object, indent=0) : # recursively maps an object
    object_type = ''
    contents = ' '
    tabs = ''
    for i in range (0, indent) :
        tabs += '\t'
    if object is None :
        object_type = 'NoneType'
        return object_type
    if isinstance(object, int) :
        object_type = 'int'
        return object_type
    elif isinstance(object, float) :
        object_type = 'float'
        return object_type
    elif isinstance(object, str) :
        object_type = 'str'
        return object_type
    elif isinstance(object, list) :
        object_type = '\n' + tabs + 'list['
        closer = ']'
        if len(object) == 0: 
            return object_type + "**empty**" + closer
        else:
            contents = ' '
            for i in object :
                contents += map_object(i, indent + 1) + ', '
            return object_type + contents + '\n' + tabs + closer
    elif isinstance(object, dict) :
        object_type = '\n' + tabs + 'dict{'
        closer = '}'
        if len(object) == 0: 
            return object_type + "**empty**" + closer
        else:
            contents = ' '
            for i in object :
                contents += map_object(object[i], indent + 1) + ', '
            return object_type + contents + '\n' + tabs + closer
    elif isinstance(object, tuple) :
        object_type = '\n' + tabs + 'tuple('
        closer = ')'
        if len(object) == 0: 
            return object_type + "**empty**" + closer
        else:
            contents = ' '
            for i in object :
                contents += map_object(i, indent + 1) + ', '
            return object_type + contents + '\n' + tabs + closer
    elif isinstance(object, set) :
        object_type = '\n' + tabs + 'set{'
        closer = '}'
        if len(object) == 0: 
            return object_type + "**empty**" + closer
        else:
            contents = ' '
            for i in object :
                contents += map_object(i, indent + 1) + ', '
            return object_type + contents + '\n' + tabs + closer    
def print_response_info(r: requests.Response) :
    print(colorama.Fore.BLUE + "response status code: ", r.status_code)
    print(colorama.Fore.BLUE + "encoding: ", r.encoding)
    print(colorama.Fore.BLUE + "headers: ", r.headers)
    print(colorama.Fore.BLUE + "reason: ", r.reason)
    print(colorama.Fore.BLUE + "cookies: ", r.cookies)
    print(colorama.Fore.BLUE + "url: ", r.url)
    print(colorama.Fore.BLUE + "request: ", r.request)
    # print(colorama.Fore.BLUE + "text: ", r.text)
    if r.json() != None :
        pretty_string = json.dumps(r.json(), indent=2)
        print(colorama.Fore.BLUE + "body (in JSON): \n", pretty_string)
def print_formatted_json(string):
    pretty_string = json.dumps(string, indent=2)
    print(colorama.Fore.BLUE + "body (in JSON): \n", pretty_string)
def combine_lists(list1 : list, list2 : list):
    for i in list2 :
        if i not in list1 :
            list1.append(i)
def get_repo_id(repo_name : str, verbose=False) :
    params = {
        'private_token' : GITLAB_API_TOKEN,
        'scope' : 'projects',
        'search' : repo_name
    }

    r = session_gitlab.get(url=gitlab_url_base + '/search', params=params, verify=False)
    

    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    for i in response:
        if i['name'] == repo_name:
                return i['id']
def check_latest_pipeline_status(project_id, verbose=False) :
    id = str(project_id)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    # 'updated_after' : datetime.datetime.today().isoformat(),
    'order_by' : 'updated_at'
    }
    r = session_gitlab.get(url=gitlab_url_base + '/projects/' + id + '/pipelines', params=params, verify=False)
    params = {
    'private_token' : GITLAB_API_TOKEN
    }
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    latest_pipe_id = ''
    updated_at_value = ''
    for i in response:
        latest_pipe_id = str(i['id'])
        updated_at_value = i['updated_at']
        initial_status = i['status']
        break
    new_updated_at_value = updated_at_value
    status = ''
    # print(colorama.Fore.RED + latest_pipe_id)
    time_elapsed = 0
    while status != 'success' or status != 'failed':
        time_as_str = str(time_elapsed)
        r = session_gitlab.get(url=gitlab_url_base + '/projects/' + id + '/pipelines/' + latest_pipe_id, params=params, verify=False)
        # if verbose == True :
        #     print_response_info(r)
        response = json.loads(r.text)
        # print(colorama.Fore.BLUE + 'response', response)
        new_updated_at_value = response['updated_at']
        status = str(response['status'])
        if status == 'success': 
            print(colorama.Fore.CYAN + "pipeline status: ", colorama.Fore.GREEN + status, colorama.Fore.CYAN + "updated_ at: ", new_updated_at_value, colorama.Fore.CYAN + "time elapsed: (" + time_as_str + 's)')
            return 0
        if status == 'failed': 
            print(colorama.Fore.CYAN + "pipeline status: ", colorama.Fore.RED + status, colorama.Fore.CYAN + "updated_ at: ", new_updated_at_value, colorama.Fore.CYAN + "time elapsed: (" + time_as_str + 's)')
            return 1
        else :
            print(colorama.Fore.CYAN + "pipeline status: ", colorama.Fore.BLUE + status, colorama.Fore.CYAN + "updated_ at: ", new_updated_at_value, colorama.Fore.CYAN + "time elapsed: (" + time_as_str + 's)')
        time.sleep(10)
        time_elapsed+=10
def create_merge_request(project_id, reviewer_ids : list, assignee = None, verbose=False) :
    id = str(project_id)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'source_branch' : 'tf-upgrade-1.0.0',
    'target_branch' : 'master',
    'title' : 'added versions.tf',
    'reviewer_ids' : json.dumps(reviewer_ids)
    }
    if assignee != None :
        params.update({'assignee_id' : assignee})

    r = session_gitlab.post(url=gitlab_url_base + '/projects/' + id + '/merge_requests', params=params, verify=False)
    response = json.loads(r.text)
    print("merge request created at ")
    print("web url : ", response['web_url'])
    if verbose == True :
        print_response_info(r)
def update_merge_request(project_id, reviewer_ids : list, assignee=None, verbose=False) :
    id = str(project_id)
    merge_request_id = '10'
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'reviewer_ids' : json.dumps(reviewer_ids)
    }
    if assignee != None :
        params.update({'assignee_id' : assignee})
    # for index in range(0, len(reviewer_ids)):
    #     field = reviewer_ids[index]
    #     params[f"reviewer_ids[{index}]"] = field

    r = session_gitlab.put(url=gitlab_url_base + '/projects/' + id + '/merge_requests/' + merge_request_id, params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
def get_current_user_open_merge_requests(verbose=False) :
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'order_by' : 'created_at',
    'state' : 'opened',
    'per_page' : 100
    }

    r = session_gitlab.get(url=gitlab_url_base + '/merge_requests', params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response
def get_merge_request(project_id : str, merge_request_iid : str, verbose=False) :
    project_id = str(project_id)
    merge_request_iid = str(merge_request_iid)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'order_by' : 'created_at',
    'state' : 'opened',
    'per_page' : 100
    }

    r = session_gitlab.get(url=gitlab_url_base + '/projects/' +  project_id + '/merge_requests/' + merge_request_iid, params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response
def accept_merge(project_id, merge_request_iid, verbose=False) :
    project_id = str(project_id)
    merge_request_iid = str(merge_request_iid)

    params = {
    'private_token' : GITLAB_API_TOKEN,
    }

    r = session_gitlab.put(url=gitlab_url_base + '/projects/' +  project_id + '/merge_requests/' + merge_request_iid + '/merge', \
        params=params, verify=False)
    print(colorama.Fore.BLUE + "response status code: ", r.status_code)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response
def get_merge_request_approvals(project_id, merge_request_iid, verbose=False) :
    project_id = str(project_id)
    merge_request_iid = str(merge_request_iid)

    params = {
    'private_token' : GITLAB_API_TOKEN,
    }

    r = session_gitlab.get(url=gitlab_url_base + '/projects/' +  project_id + '/merge_requests/' + merge_request_iid + '/approvals', \
        params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response
def get_project_merge_request(project_id, verbose=False) :
    id = str(project_id)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'order_by' : 'created_at',
    }

    r = session_gitlab.get(url=gitlab_url_base + '/projects/' + id + '/merge_requests', params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
def get_project_attribute(project_id, paramter : str, verbose=False) :
    id = str(project_id)
    params = {
    'private_token' : GITLAB_API_TOKEN
    }

    r = session_gitlab.get(url=gitlab_url_base + '/projects/' + id, params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response[paramter]
def get_project_merge_request_approvals_config(project_id, verbose=False) :
    id = str(project_id)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'order_by' : 'created_at',
    }

    r = session_gitlab.get(url=gitlab_url_base + '/projects/' + id + '/approval_rules', params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response
def get_group_subgroups_ids(group_id, verbose=False) :
    id = str(group_id)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'all_available' : True
    }

    r = session_gitlab.get(url=gitlab_url_base + '/groups/' + id + '/subgroups', params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    subgroup_ids = []
    for i in response :
        if verbose == True:
            print(i['id'])
        subgroup_ids.append(i['id'])
    if len(subgroup_ids) == 0 :
        return None
    return subgroup_ids
def get_group_project_ids(group_id, recursive=True, verbose=False) :
    id = str(group_id)  
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'per_page' : 100
    }

    r = session_gitlab.get(url=gitlab_url_base + '/groups/' + id + '/projects', params=params, verify=False)
  
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    project_ids = []
    for i in response :
        project_ids.append(i['id'])
    if recursive == True :
        subgroups = get_group_subgroups_ids(id)
        if subgroups != None :
            for i in subgroups :
                project_ids += get_group_project_ids(i)    
    if len(project_ids) == 0 :
        return None
    return(project_ids)
def get_group_id(search_term, verbose=False) :
    search_term = str(search_term)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    'search' : search_term
    }
    r = session_gitlab.get(url=gitlab_url_base + '/groups',  params=params, verify=False)
    params = {
    'private_token' : GITLAB_API_TOKEN
    }
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
def get_group_members(group_id, verbose=False) :
    id = str(group_id)
    params = {
    'private_token' : GITLAB_API_TOKEN,
    }
    r = session_gitlab.get(url=gitlab_url_base + '/groups/' + id + '/members', params=params, verify=False)
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    member_ids = []
    for i in response : 
        if verbose == True :
            print(i['name'], ':', i['id'])
        member_ids.append(i['id'])
    return member_ids
def get_my_user_id(verbose=False) :
    params = {
    'private_token' : GITLAB_API_TOKEN,
    }
    r = session_gitlab.get(url=gitlab_url_base + '/user', params=params, verify=False)
    if verbose == True :
        print_response_info(r)
    response = json.loads(r.text)
    return response['id'] 
def has_two_approvals(request_response : dict) :
    if len(request_response['approved_by']) >= 2:
        return True
    return False
def audit_group_approval_configs(group_id, verbose=False) :
    project_ids = get_group_project_ids(group_id)
    configs = []
    for i in project_ids :
        try:
            configs.append({get_project_attribute(i, 'name_with_namespace'), get_project_merge_request_approvals_config(i, verbose=True)})
        except KeyError:
            configs.append({get_project_attribute(i, 'name_with_namespace'), None})
    return configs

def main() :

    project_ids = get_group_project_ids(sys.argv[1])
    for i in project_ids :
        output = subprocess.check_output(['git', 'clone', get_project_attribute(i, 'ssh_url_to_repo')])
        print(output)

if __name__ == '__main__':
    main()
    

